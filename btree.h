#ifndef BTREE_H
#define BTREE_H

/******* Definicao da BTree ***********/

/* ordem da arvore (modifique aqui para testar outros valores de T) */
#define T 4

typedef struct BTNode{
    int keys[2*T+1];
    struct BTNode* ptChildren[2*T+2];
    int isLeaf;
    int numKeys;
} BTNode;

typedef struct{
    BTNode* root;
} BTree;


/******* TODO: implementar esses metodos ***********/

void initializeBTree(BTree* bt);

void insertBTree(BTree* bt, int key);

int insertInChildBTree(BTNode* x, int key);

void splitChildrenBTree(BTNode* x, int i);


/************ Metodos auxiliares *******************/

void printBTree(BTree* bt);
void printBTNode(BTNode* n, int level, int min, int max);

int checkBTree(BTree* bt);
int checkBTNode(BTNode* n, int min, int max);

void createRandomArray(int* C, int size);

#endif // BTREE_H
