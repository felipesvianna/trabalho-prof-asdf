#include "btree.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

void initializeBTree(BTree* bt)
{
    BTNode* r;

    /* TODO: Alocar um nodo e fazer a raiz da arvore apontar para o nodo gerado */
	int i;

    /* TODO: Alocar um nodo e fazer a raiz da arvore apontar para o nodo gerado */
	r = (BTNode*)malloc(sizeof(BTNode));
	r->isLeaf = 1;
	r->numKeys = 0;

	for(i=0;i<(2*T+1);i++)
		r->keys[i] = 0;

	for(i=0;i<(2*T+2);i++)
		r->ptChildren[i] = NULL;

	bt->root = r;
}

void insertBTree(BTree* bt, int key)
{
    BTNode* r = bt->root;
    BTNode* novo;
    int overflow;
    /* TODO: Inserir key na arvore a partir da raiz.
             Descer pela arvore recursivamente ate chegar em uma folha usando o metodo 'insertInChildBTree'.
             Usar metodo 'splitChildrenBTree' caso seja detectado overflow na raiz */

    overflow = insertInChildBTree(r, key);

    if(overflow)
    {
        novo = (BTNode*)malloc(sizeof(BTNode));
        bt->root = novo;
        novo->isLeaf = 0;
        novo->numKeys = 0;
        novo->ptChildren[0] = r;
        splitChildrenBTree(novo, 0);
    }
}

int insertInChildBTree(BTNode* x, int key)
{
    /* TODO: Inserir key no nodo x caso ele seja uma folha,
             ou descer recursivamente pelo filho adequado ate chegar em uma folha.
             Usar 'splitChildrenBTree' caso seja detectado overflow no nodo x apos insercao.

       IMPORTANTE: retornar 1 se houver overflow, ou 0 no caso contrario. */
    int overflow, t, i;
    BTNode* filho = (BTNode*)malloc(sizeof(BTNode));

    i = x->numKeys;

    if(x->isLeaf)
    {
        while(i >= 0 && key < x->keys[i]){
            x->keys[i+1] = x->keys[i];
            i--;
        }

        x->keys[i+1] = key;
        x->numKeys = x->numKeys+1;
    }else{
        while(i>=0 && key < x->keys[i])
            i--;

        filho = x->ptChildren[i+1];
        overflow = insertInChildBTree(filho, key);

        if(overflow)
            splitChildrenBTree(x, i+1);
    }

    //inicializa t
    t = (T);

    if(x->numKeys > 2*t)
        return 1;
    else
        return 0;
}

void splitChildrenBTree(BTNode* x, int i)
{
    /* TODO: Fazer particionamento do i-esimo filho do nodo x em dois nodos */
    BTNode* novo;
    BTNode* filho;
    novo = (BTNode*)malloc(sizeof(BTNode));
    filho = (BTNode*)malloc(sizeof(BTNode));
    int t,j;

    filho = x->ptChildren[i];

    //inicializa t
    t = (T);

    novo->isLeaf = filho->isLeaf;
    novo->numKeys = t;

    for(j=0;j<t;j++)
        novo->keys[j] = filho->keys[j+t+1];

    if(filho->isLeaf == 0){
        for(j=0;i<t+1;j++)
            novo->ptChildren[j] = filho->ptChildren[j+t+1];
    }

    filho->numKeys = t;

    for(j=x->numKeys+1;j>=i+1;j--)
        x->ptChildren[j+1] = x->ptChildren[j];

    x->ptChildren[i+1] = filho;

    for(j=x->numKeys;j>=i;j--)
        x->keys[j+1] = x->keys[j];

    x->keys[i] = filho->keys[t];
    x->numKeys = x->numKeys+1;
}

/**************************************************************************/
/**************************************************************************/
/**************************************************************************/

/* Percorre a arvore recursivamente,
* testando se os valores das chaves satisfazem
* os limites superiores e inferiores de cada nodo
*/
int checkBTree(BTree* bt)
{
    return checkBTNode(bt->root, -INT_MAX, INT_MAX);
}

int checkBTNode(BTNode* n, int min, int max)
{
    int i;
    for(i=0;i<n->numKeys;i++)
        if(n->keys[i] <= min || n->keys[i] >= max)
            return -1;

    int newMin = min;
    int newMax = n->keys[0];
    if(n->isLeaf == 0){
        for(i=0;i<n->numKeys+1;i++){
            int check = checkBTNode(n->ptChildren[i],newMin,newMax);
            if(check == -1)
                return -1;

            newMin = newMax;
            if(i+1 < n->numKeys)
                newMax = n->keys[i+1];
            else
                newMax = max;
        }
    }

    return 1;
}

/* Percorre a arvore recursivamente,
* imprimindo o nivel em que cada nodo se encontra,
* os limites superior e inferior de cada nodo
* e os valores das chaves contidas nos nodos
*/
void printBTree(BTree* bt)
{
    printf("BTree:\n");
    printBTNode(bt->root, 0, -INT_MAX, INT_MAX);
}

void printBTNode(BTNode* n, int level, int min, int max)
{
    printf( "%*s", level, "" );
    if(min==-INT_MAX && max==INT_MAX)
        printf("nivel %d lim <-inf,inf> chaves ",level);
    else if(min==-INT_MAX && max!=INT_MAX)
        printf("nivel %d lim <-inf,%d> chaves ",level,max);
    else if(min!=-INT_MAX && max==INT_MAX)
        printf("nivel %d lim <%d,inf> chaves ",level,min);
    else
        printf("nivel %d lim <%d,%d> chaves ",level,min,max);

    int i;
    for(i=0;i<n->numKeys;i++)
        printf("%d ",n->keys[i]);
    printf("\n");

    int newMin = min;
    int newMax = n->keys[0];
    if(n->isLeaf == 0){
        for(i=0;i<n->numKeys+1;i++){
            printBTNode(n->ptChildren[i],level+1,newMin,newMax);
            newMin = newMax;
            if(i+1 < n->numKeys)
                newMax = n->keys[i+1];
            else
                newMax = max;
        }
    }
}

/* Gera array de inteiros aleatorios (sem repeticao) de tamanho size */
void createRandomArray(int* C, int size)
{
    int i;
    for(i=0; i<size; i++)
        C[i] = i;

    int s;
    for(s=size; s>0; s--){
        int index = rand()%s;

        /* troca C[index] com C[s-1] */
        int temp = C[s-1];
        C[s-1] = C[index];
        C[index] = temp;
    }
}
