#ifndef HASH_H
#define HASH_H

/******* Definicao da Hash ***********/

/* tamanho da tabela (modifique aqui para testar outros valores de M) */
#define M 100
#define strSize 25

/* definicao do elemento da tabela, composto pela chave e dados satelites */
typedef struct
{
    char key[strSize];
    char data[strSize];
} element;

/* definicao da tabela hash */
typedef struct{

    /* TODO: Inserir os componentes necessarios para a tabela
             (depende do tratamento de colisoes escolhido)
             ex: array de flags, array de ponteiros para listas, etc */
    int tamanho;
    int difEndereco[M];
    element tabela[M];

} HashTable;

/******* TODO: implementar esses metodos ***********/

void initializeHashTable(HashTable* ht);

int computeHash(HashTable* ht, const char key[strSize], int incrDoubleHash);

int insertHash(HashTable* ht, element elem);

int searchHash(HashTable* ht, element* elem);


/************ Metodos auxiliares *******************/

int splitString(char* str, element* elem);

#endif // HASH_H
