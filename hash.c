#include <stdlib.h>
#include "hash.h"
#include "string.h"

/* inicializa tabela hash */
void initializeHashTable(HashTable* ht)
{
    /* TODO: inicializar valores da tabela
             (depende da implementacao escolhida)
             ex: setar flags como 0, ponteiros como 0 */
    int i;

    ht->tamanho = M;

    for(i=0;i<M;i++)
    {
        ht->difEndereco[i] = 0;
        ht->tabela[i].key[0] = '\0';
        ht->tabela[i].data[0] = '\0';
    }
}

/* computa a funcao de hash dada a string key
* --------------------------------
* retorna um valor inteiro entre 0 e M-1
*/
int computeHash(HashTable* ht, const char key[strSize], int incrDoubleHash)
{
    int hash=0, prehash;
    int i;
    int tam_str = strlen(key);

    /* TODO: implementar funcao de hash */

    for (i=0;i<tam_str;i++)
        hash = (31 * hash + key[i]) % ht->tamanho + (incrDoubleHash * (7 * hash + key[i]) % ht->tamanho);

    return hash;
}

/* insere elemento na tabela hash
* --------------------------------
* computa a função de hash sobre a chave em elem.key
* e trata eventuais colisões durante inserção
* se não conseguiu inserir o elemento, retorna -1
* caso contrario, retorna numero de colisoes
*/
int insertHash(HashTable* ht, element elem)
{
    /* TODO: implementar tratamento de colisoes (e contar a quantidade de colisoes)
             Lembrar de usar strcpy(dst,src) para copiar os valores de elem.key e elem.data para a tabela */

    int numCollisions = 0, collision;
    int incrDoubleHash, hash, firstHash;

    incrDoubleHash=0;

    do
    {
        collision=0;
        //printf("%s\n", elem.key);
        hash = computeHash(ht,elem.key,incrDoubleHash);

        if(incrDoubleHash == 0)
            firstHash = hash;

        //Verifica se há colisao na tabela
        if(strlen(ht->tabela[hash].data) != 0)
        {
            incrDoubleHash++;
            numCollisions++;
            collision = 1;
            ht->difEndereco[firstHash] = incrDoubleHash;
        }
        else{
            strcpy(ht->tabela[hash].key, elem.key);
            strcpy(ht->tabela[hash].data, elem.data);
        }
    }while(collision == 1);

    return numCollisions;
}

/* busca elemento na tabela hash
* ------------------------------
* computa a função de hash sobre a chave em elem->key
* busca na tabela considerando uma resolução de conflitos adequada
* se encontrou elemento, copia dados associados a chave em elem->data e retorna 1
* caso contrario, retorna 0
*/
int searchHash(HashTable* ht, element* elem)
{
    /* TODO: implementar busca na tabela, comparando elem->key com os valores de chaves contidos na tabela
             Para a comparação das chaves, usar strcmp(str1,str2), que retorna 0 quando as strings sao iguais
             Quando encontrar a chave, usar strcpy(dst,src) para copiar o valor adequado para elem->data */

    int found=0;
    int incrDoubleHash=0;
    int totalComparacoes=1;

    while(totalComparacoes<=2 && found == 0){
        int hash = computeHash(ht,elem->key,incrDoubleHash);

        printf("%s %s %d\n",ht->tabela[hash].key, ht->tabela[hash].data, ht->difEndereco[hash]);

        if(strcmp(ht->tabela[hash].key, elem->key) == 0)
        {
            strcpy(elem->data, ht->tabela[hash].data);
            found = 1;
        }
        else if(ht->difEndereco[hash] > 0)
        {
            incrDoubleHash = ht->difEndereco[hash];
        }

        //printf("%s %s %d\n",ht->tabela[hash].key, ht->tabela[hash].data, ht->difEndereco[hash]);

        totalComparacoes++;
    }
    return found;
}

/**************************************************************************/
/**************************************************************************/
/**************************************************************************/

int splitString(char* str, element* elem)
{
    int i=0,j=0;
    while(str[i]!='\0' && str[i]!=';'){
        elem->key[i]=str[i];
        i++;
    }
    elem->key[i]='\0';

    i++;
    while(str[i]!='\0' && str[i]!='\n'){
        elem->data[j]=str[i];
        i++;
        j++;
    }
    elem->data[j]='\0';
}
