#include <stdio.h>  /* printf */
#include <stdlib.h> /* malloc */
#include <string.h> /* strlen */
#include <time.h>

#include "hash.h"
#include "btree.h"

void testaHash()
{
    element elem;
    int i, numCollisions, totalCollisions=0, found;

    /* Inicializa tabela hash */
    HashTable ht;
    initializeHashTable(&ht);

    /* Abre arquivo do dataset */
    FILE* fp = fopen("dataset.txt", "r");
    if (fp == NULL) {
      fprintf(stderr, "Não foi possível abrir dataset!\n");
      return;
    }

    /* Le arquivo do dataset inserindo os nomes na hash */
    printf("---------\n TABELA \n---------\n\n");
    i=0;
    char str[2*strSize];
    while (fgets(str, 2*strSize, fp)) {
        /* separa as duas strings de cada linha do dataset em 'key' e 'data'*/
        splitString(str,&elem);

        /* insere elemento na hash, e retorna numero de colisoes durante a insercao */
        numCollisions = insertHash(&ht,elem);

        totalCollisions += numCollisions;

        /* imprime key, data e numero de colisoes */
        printf("key: %-20s| data: %-22s | colisoes %d\n", elem.key,elem.data,numCollisions);

        i++;
    }
    fclose(fp);

    /* Abre arquivo de queries */
    fp = fopen("queries.txt", "r");
    if (fp == NULL) {
      fprintf(stderr, "Não foi possível abrir arquivo de queries!\n");
      return;
    }

    elem.data[0]='\0';
    /* Le arquivo de queries buscando os nomes na hash */
    printf("\n-----------\n CONSULTAS\n-----------\n\n");
    while (fgets(elem.key, strSize, fp)) {
        elem.key[strlen(elem.key)-1]='\0';

        printf("Busca %s\n", elem.key);

        /* procura elemento na hash por 'elem.key'
           se encontrar atualiza o valor de 'elem.data' e retorna 1,
           caso contrario retorna 0 */
        found = searchHash(&ht,&elem);

        /* imprime se achou ou nao o elemento na tabela hash */
        if(found)
            printf("%s é %s\n",elem.key,elem.data);
        else
            printf("ERRO: a identidade de %s é desconhecida!\n",elem.key);

        printf("\n");
    }
    fclose(fp);

    printf("\n---------------------\n");
}

void testaBTree()
{
    int i;
    BTree btree;

    /* inicializa B-Tree como uma arvore vazia */
    initializeBTree(&btree);

    /* nesta impressao, arvore deve conter apenas a raiz */
    printBTree(&btree);


    /* gera array com 'numElements' valores aleatorios
       OBS: varie o valor de 'numElements' para testar com mais valores */
    int numElements = 5;
    //int* C = malloc(numElements*sizeof(int));
    //createRandomArray(C,numElements);

    int C[5] = {10,20,30,40,35};

    /* insere os valores na arvore */
    for(i=0;i<numElements;i++){
        insertBTree(&btree,C[i]);

        /* se quiser ver passo-a-passo o estado da arvore, descomente as linhas abaixo */

        printf("insere %d\n",C[i]);
        printBTree(&btree);

    }

    /* imprime versao final da arvore */
    printf("FINAL:\n");
    printBTree(&btree);

    /* checa se arvore eh valida */
    if(checkBTree(&btree)==1)
        printf("BTree valida!\n");
    else
        printf("BTree invalida!\n");
}

int main()
{
    srand(time(NULL));

    //testaHash();

    testaBTree();

    return 0;
}
