void initializeBTree(BTree* bt)
{
    BTNode* r;
	int i;

    /* TODO: Alocar um nodo e fazer a raiz da arvore apontar para o nodo gerado */
	r = malloc(numElements*sizeof(BTnode));
	r->isLeaf = 1;
	r->numKeys = 0;
	
	for(i=0;i<(2*T+1);i++)
		r->keys[i] = 0;
	
	for(i=0;i<(2*T+2);i++)
		r->ptChildren[i] = NULL;
	
	bt = r;

}

void insertBTree(BTree* bt, int key)
{
    BTNode* r = bt->root;

    /* TODO: Inserir key na arvore a partir da raiz.
             Descer pela arvore recursivamente ate chegar em uma folha usando o metodo 'insertInChildBTree'.
             Usar metodo 'splitChildrenBTree' caso seja detectado overflow na raiz */




}

int insertInChildBTree(BTNode* x, int key)
{
    /* TODO: Inserir key no nodo x caso ele seja uma folha,
             ou descer recursivamente pelo filho adequado ate chegar em uma folha.
             Usar 'splitChildrenBTree' caso seja detectado overflow no nodo x apos insercao.

       IMPORTANTE: retornar 1 se houver overflow, ou 0 no caso contrario. */




}

void splitChildrenBTree(BTNode* x, int i)
{
    /* TODO: Fazer particionamento do i-esimo filho do nodo x em dois nodos */





}